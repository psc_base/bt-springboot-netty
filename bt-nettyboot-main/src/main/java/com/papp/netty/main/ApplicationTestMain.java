package com.papp.netty.main;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationTestMain {

    private static final Logger logger = LogManager.getLogger(ApplicationTestMain.class);

    private static Socket socket = null;

    public static void main(String[] args) {
        setUp();
        if (socket != null) {
            try (BufferedWriter output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "8859_1"))) {
                for (int i = 0; i < 1; i++) {
                    output.write("PRA!XML!TEST#END#");
                    output.flush();
                    output.write("MSG!<TEST>Test Netty</TEST>#END#]");
                    output.flush();
                }
            } catch (Exception ex) {
                logger.error(" Before Closing Connection : ", ex);
            } finally {
                clear();
            }
        }

    }

    public static void setUp() {
        try {
            socket = new Socket("localhost", 9001);
            socket.setSoTimeout(30000);
        } catch (IOException e) {
            logger.error(" Exception while  Connecting with server1.", e);
        }
    }

    public static void clear() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.error("Error in closing socket connection : ", e);
        }
    }
}
