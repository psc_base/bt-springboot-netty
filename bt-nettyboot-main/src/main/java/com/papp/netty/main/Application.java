package com.papp.netty.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.papp.netty.server.NettyServer;

@SpringBootApplication
@ComponentScan(basePackages = { "com.papp.netty.server", "com.pweb.netty.*" })
public class Application {

    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        final ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        NettyServer inboundServer = ctx.getBean("nettyServer", NettyServer.class);
        try {
            inboundServer.run();
        } catch (Exception e) {
            logger.error("Error in Netty ==>> ", e);
        }
    }
}
