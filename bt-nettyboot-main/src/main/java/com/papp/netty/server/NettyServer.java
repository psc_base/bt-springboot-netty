package com.papp.netty.server;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.papp.netty.handler.DMDecoder;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

@Component(value="nettyServer")
public class NettyServer {
	
	private static final Logger logger = LogManager.getLogger(NettyServer.class); 
	
	public void run() throws InterruptedException {

		final ByteBuf[] endLn = {Unpooled.buffer("#END#]".length()), Unpooled.buffer("#END#".length())};
		endLn[0].writeBytes("#END#]".getBytes());
		endLn[1].writeBytes("#END#".getBytes());
		if(logger.isInfoEnabled()) {
		    logger.info("Server starting");
		}
		int bossThreadCount = 1;
		int workerThreadCount = 10;
		int executionHandlerCount = 80;

		EventLoopGroup bossGroup = new NioEventLoopGroup(bossThreadCount); // (1)
		EventLoopGroup workerGroup = new NioEventLoopGroup(workerThreadCount);

		((NioEventLoopGroup) workerGroup).setIoRatio(90);
		
		final EventExecutorGroup execHandler = new DefaultEventExecutorGroup(executionHandlerCount);

		try {
			ServerBootstrap bootstrap = new ServerBootstrap();
			bootstrap.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.handler(new LoggingHandler(LogLevel.ERROR))
					.option(ChannelOption.TCP_NODELAY, true)
					.option(ChannelOption.SO_KEEPALIVE, true)
					.option(ChannelOption.SO_REUSEADDR, true)
					.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 50)
					.option(ChannelOption.SO_BACKLOG, 100)
					.childHandler(new ChannelInitializer<SocketChannel>() {

						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							ch.pipeline().addLast("frameDecoder", 
												   new DMDecoder(2999, false, endLn));
							
							ch.pipeline().addLast("idleStateHandler",
										  		   new IdleStateHandler(0, 0, 1000, TimeUnit.MILLISECONDS));

							ch.pipeline().addLast(execHandler,
												  new NettyHandler());

						}
					});
			if(logger.isInfoEnabled()) {
			    logger.info("server start successfully");
			}
			bootstrap.bind(9001).sync().channel().closeFuture().sync();
			
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
