package com.papp.netty.server;

import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;

public class NettyHandler extends ChannelDuplexHandler {
	private static final Logger logger = LogManager.getLogger(NettyHandler.class);

	private String sourceIP = "";
	private String sourcePort = "";
	String localPort = "";
	String localIP = "";

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		String localAddress = ctx.channel().localAddress().toString();
		localPort = localAddress.substring(localAddress.indexOf(':') + 1);
		localIP = localAddress.substring(1, localAddress.indexOf(':'));
		String sourceAdd = ctx.channel().remoteAddress().toString();
		sourceIP = sourceAdd.substring(sourceAdd.indexOf('/') + 1, sourceAdd.indexOf(':'));
		sourcePort = sourceAdd.substring(sourceAdd.indexOf(':') + 1);
		if(logger.isInfoEnabled()) {
		    logger.info("Channel Active :=> " + localPort + " : " + localIP + " : " + sourceIP + " : " + sourcePort);
		}
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
	    if(logger.isInfoEnabled()) {
	        logger.info("Channel disconnected");
	    }
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		ByteBuf cbReq = (ByteBuf) msg;
		StringBuilder sbReq = new StringBuilder();
		sbReq.append(cbReq.toString(Charset.defaultCharset()).trim());
		if(logger.isInfoEnabled()) {
		    logger.info("Request from " + sourceIP + ":" + sbReq.toString());
		}
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
	    if(logger.isInfoEnabled()) {
	        logger.info("Child Channel Idle, closing connection");
	    }
		ctx.channel().close(); 
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable e) {
		logger.error("Unexpected exception from downstream.", e);
		ctx.channel().close();
	}

}