package com.papp.netty.handler;

import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

public class DMDecoder extends DelimiterBasedFrameDecoder {
	
	private static final Logger logger = LogManager.getLogger(DMDecoder.class);
	private static AtomicInteger counter = new AtomicInteger();
	public DMDecoder(int maxFrameLength, boolean stripDelimiter, ByteBuf delimiter) {
		super(maxFrameLength, stripDelimiter, delimiter);
	}
	
	public DMDecoder(int maxFrameLength, boolean stripDelimiter, ByteBuf[] delimiter) {
		super(maxFrameLength, stripDelimiter, delimiter);
	}

	@Override
	protected Object decode(ChannelHandlerContext arg0, ByteBuf arg2) throws Exception {
		
		logger.info("Decode :=> " + counter.getAndIncrement());
		ByteBuf cbFrame = (ByteBuf) super.decode(arg0, arg2);
		
		if (cbFrame == null) {
			return null;
		}
		String frame = cbFrame.toString(Charset.defaultCharset());
		// If we want to do any operation with input string message, we can do here
		return getChannelBuffer(frame);
	}

	private ByteBuf getChannelBuffer(String strReq) {
		ByteBuf time = Unpooled.buffer(strReq.length());
		time.writeBytes(strReq.getBytes());
		return time;
	}
}
